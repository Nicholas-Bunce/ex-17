﻿using System;

namespace ex_17
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Is -1 bigger than -10 Yes or No?");
            var decision = (Console.ReadLine());

            if (decision == ("Yes") || decision == ("yes"))
        {
            Console.WriteLine("You have chosen correctly");
        }
        else
        {
            Console.WriteLine("You have chosen incorrectly");
        }
        
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
        }
    }
}
